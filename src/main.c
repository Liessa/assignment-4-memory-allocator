#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>

#define MALLOC_SIZE 128

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}
static struct block_header *bl_init(){
    struct block_header *block = heap_init(REGION_MIN_SIZE);
    assert(block);
    return block;
}

void test_alloc_region() {
    printf("test alloc region:\n");
    bl_init();

    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    assert(block1->is_free != true);
    assert(block1->capacity.bytes == MALLOC_SIZE);

    heap_term();
}

void test_free_one() {
    bl_init();

    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    void *ptr1 = _malloc(MALLOC_SIZE);
    struct block_header *block2 = block_get_header(ptr1);
    void *ptr2 = _malloc(MALLOC_SIZE);
    struct block_header *block3 = block_get_header(ptr2);

    _free(block3);
    assert(block1->is_free != true);
    assert(block2->is_free != true);
    assert(block3->is_free == true);

    heap_term();
}

void test_free_two() {
    printf("test free two:\n");
    bl_init();

    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);
    void *ptr1 = _malloc(MALLOC_SIZE);
    struct block_header *block2 = block_get_header(ptr1);
    void *ptr2 = _malloc(MALLOC_SIZE);
    struct block_header *block3 = block_get_header(ptr2);

    _free(block3);
    assert(block1->is_free != true);
    assert(block2->is_free != true);
    assert(block3->is_free == true);

    _free(block2);
    assert(block1->is_free != true);
    assert(block2->is_free == true);
    assert(block3->is_free == true);

    heap_term();
}

void test_new_region_grow() {
    printf("test new region grow:\n");
    bl_init();

    void *ptr = _malloc(MALLOC_SIZE);
    struct block_header *block1 = block_get_header(ptr);

    assert(block1->capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(block1->capacity).bytes > REGION_MIN_SIZE);

    heap_term();
}

int main() {
    test_alloc_region();
    test_free_one();
    test_free_two();
    test_new_region_grow();
    return 0;
}
